# Exercice X: Bonus (pour les gens qui vont très, très vite, démarrer de la branche `exercice06`)

## Formulaires: contrôles avancés

* Remplacer la textarea par un éditeur de texte riche.
* Remplacer le champ date par un datepicker (en modifiant `gtdDate`)

## Formulaires: validation personnalisée

* Permettre de définir un validateur avec un expression Angular dans `gtdOnError`. L'intégrer avec la validation de `ngModelController` ($parsers, $setValidity).
* La même, mais avec un allez-retour serveur (utiliser `$q`, ce type de validation est intégré depuis Angular 1.3 avec un état de validation "pending" et des `$asyncValidators`).
* Permettre de définir un état "warning" sur un champ: marquer des erreurs somme "pas graves", les montrer avec `has-warning` au lieu de `has-error`, potentiellement demander avant soumission du formulaire (popup "êtes-vous sûr ?", configurable).
