# Exercice 2: Afficher une liste de tâches (démarrer de la branche `exercice01`)

## Initialiser la page d'accueil de l'application.

1. Ajouter *home/home.js* aux scripts chargés par *index.html*
1. [Configurer la route](https://code.angularjs.org/1.4.7/docs/api/ngRoute/provider/$routeProvider#when) dans *home/home.js*
1. Ajouter [les liens vers la home](https://code.angularjs.org/1.4.7/docs/api/ng/directive/ngHref) dans la navbar de *index.html*
1. Créer un contrôleur `HomeController` dans *home/home.js*.
1. Charger le contrôleur dans le template *home/home.html*.
1. Charger une liste de tâches dans le scope. [Les tâches sont en bas de l'exercice.](#tasks)
1. Binder les tâches dans *home/home.html* et afficher leur titre
1. Lier les tâches à leurs URLs respectives. L'URL est "`#/tasks/:taskId`" où "`:taskId`" est l'ID de la tâche.
1. [Cacher](https://code.angularjs.org/1.4.7/docs/api/ng/directive/ngHide) ou [supprimer](https://code.angularjs.org/1.4.7/docs/api/ng/directive/ngIf) la liste s'il n'y a pas de tâches.

## Utiliser un service pour fournir les tâches

On vous fournit un serveur (`npm start`). Ce serveur vous fournit la liste de tâches en REST.

1. Créer le service `Task` dans *common/taskService.js*
1. Utiliser [ngResource](https://code.angularjs.org/1.4.7/docs/api/ngResource/service/$resource) pour appeler le serveur. L'URL est `rest/tasks/:taskId`. Que représente `:taskId` ? Binder `:taskId` à l'ID de la tâche.
1. Injecter le service dans le contrôleur et l'utiliser à la place de la liste statique
1. Lancer. Ça ne marche pas. Pourquoi ? Réparer.


On a notre liste de tâches, filtrons la !


## Les tâches

<pre id="tasks">
{
    1: {
        id: 1,
        title: 'Une tâche',
        plannedFor: Date.now() - 86400000,
        duration: 5,
    },
    2: {
        id: 2,
        title: 'Une autre tâche',
        plannedFor: Date.now() + 86400000,
        duration: 1,
    },
    3: {
        id: 3,
        title: 'Tâche de remplissage',
        plannedFor: Date.now() + 86400000,
        duration: 60,
    },
    4: {
        id: 4,
        title: 'Tâche de remplissage',
        plannedFor: Date.now() + 86400000,
        duration: 60,
    },
    5: {
        id: 5,
        title: 'Tâche de remplissage',
        plannedFor: Date.now() + 86400000,
        duration: 60,
    },
    6: {
        id: 6,
        title: 'Tâche de remplissage',
        plannedFor: Date.now() + 86400000,
        duration: 60,
    },
    7: {
        id: 7,
        title: 'Tâche de remplissage',
        plannedFor: Date.now() + 86400000,
        duration: 60,
    },
    8: {
        id: 8,
        title: 'Tâche de remplissage',
        plannedFor: Date.now() + 86400000,
        duration: 60,
    },
    9: {
        id: 9,
        title: 'Tâche de remplissage',
        plannedFor: Date.now() + 86400000,
        duration: 60,
    }
}
</pre>
